package paoo.pc.tp2.tp2fxchantier;

import java.time.Instant;
import java.time.LocalTime;
import java.util.Random;
import java.util.logging.Logger;


public class Camion {

    private int id;
    private int voyage;
    private Entrepot entrepot;
    private Chantier chantier;
    private static final Logger LOG = Logger.getLogger(Camion.class.getName());
    private static final Random r = new Random(Instant.now().getEpochSecond());


    public Camion(Entrepot entrepot, Chantier chantier) {
        this.entrepot = entrepot;
        this.chantier = chantier;
    }

    public int livrer(){
        if(this.entrepot.getStock() <= 0) {
            return 0;
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            LOG.warning(e.getMessage());
        }
        this.voyage++;
        int q = r.nextInt(1000);
        int q2 = entrepot.get(q);
        synchronized (chantier) {
            chantier.add(q2);
        }
        System.out.print(LocalTime.now());
        System.out.printf(": Camion:%d/Voyage:%d/Quantité:%d%n",
                this.id, this.voyage, q2);
        return q2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVoyage() {
        return voyage;
    }

    public void setVoyage(int voyage) {
        this.voyage = voyage;
    }

    public Entrepot getEntrepot() {
        return entrepot;
    }

    public void setEntrepot(Entrepot entrepot) {
        this.entrepot = entrepot;
    }

    public Chantier getChantier() {
        return chantier;
    }

    public void setChantier(Chantier chantier) {
        this.chantier = chantier;
    }
}
