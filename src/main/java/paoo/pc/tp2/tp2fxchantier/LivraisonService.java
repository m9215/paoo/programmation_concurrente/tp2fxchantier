package paoo.pc.tp2.tp2fxchantier;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

public class LivraisonService extends ScheduledService<Integer> {
    private Camion camion;

    public LivraisonService(Camion camion) {
        super();
        this.camion = camion;
        this.setPeriod(Duration.seconds(5));
    }

    @Override
    protected Task<Integer> createTask() {
        return new Task<>() {
            @Override
            protected Integer call() {
                return camion.livrer();
            }
        };
    }
}
