package paoo.pc.tp2.tp2fxchantier;

public class Entrepot {
    public int stock;

    public Entrepot(int stock) {
        this.stock = stock;
    }

    public int get(int n) {
        int q = n;
        if (this.stock < n) {
            q = stock;
            stock = 0;
        } else {
            stock -= n;
        }
        return q;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
