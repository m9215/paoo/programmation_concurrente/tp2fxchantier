package paoo.pc.tp2.tp2fxchantier;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.util.Duration;

public class DeplacementService extends ScheduledService<Integer> {
    private Brouette brouette;

    public DeplacementService(Brouette brouette) {
        super();
        this.brouette = brouette;
        this.setPeriod(Duration.millis(200));
    }

    @Override
    protected Task<Integer> createTask() {
        return new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                return brouette.deplacer();
            }
        };
    }
}
