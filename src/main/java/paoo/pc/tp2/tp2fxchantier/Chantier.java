package paoo.pc.tp2.tp2fxchantier;

public class Chantier {

    private int num;

    public int add(int n) {
        this.num += n;
        return this.num;
    }

    public int get(int n) {
        this.num = (this.num < n)?0:num-n;
        return this.num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
