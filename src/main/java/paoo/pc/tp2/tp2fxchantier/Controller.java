package paoo.pc.tp2.tp2fxchantier;

import javafx.concurrent.WorkerStateEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import java.time.LocalTime;

public class Controller {
    public static final int MAXBRIQUES = 2000;
    Entrepot entrepot = new Entrepot(MAXBRIQUES);
    Chantier chantier = new Chantier();
    Camion camion = new Camion(entrepot, chantier);
    Brouette brouette = new Brouette(chantier);
    LivraisonService livraisonService = new LivraisonService(camion);
    DeplacementService deplacementService = new DeplacementService(brouette);

    @FXML
    private Button bouton;
    @FXML
    private TextArea texte;

    @FXML
    public void initialize() {
        texte.appendText("Démarrage à " + LocalTime.now() + "\n");
        livraisonService.setOnSucceeded((WorkerStateEvent event) -> {
            texte.appendText("Livraison " + LocalTime.now() +
                    " -> " + livraisonService.getValue() + " briques\n");
            if (camion.getEntrepot().getStock() <= 0) {
                texte.appendText("Livraisons terminées (" + LocalTime.now() + ")\n");
                livraisonService.cancel();
            }
        });
        deplacementService.setOnSucceeded((WorkerStateEvent event) -> {
            texte.appendText("Déplacement " + LocalTime.now() +
                    " reste -> " + deplacementService.getValue() + " briques\n");
            if (brouette.getChantier().getNum() <= 0) {
                texte.appendText("Déplacements terminées (" + LocalTime.now() + ")\n");
                deplacementService.cancel();
            }
        });
    }

    @FXML
    public void demarrer() throws InterruptedException {
        livraisonService.start();
        Thread.sleep(6000);
        deplacementService.start();
    }
}