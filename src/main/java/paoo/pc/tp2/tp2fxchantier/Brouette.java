package paoo.pc.tp2.tp2fxchantier;

import java.time.LocalTime;

public class Brouette {

    private int id;
    private int voyage;
    private Chantier chantier;

    public Brouette(Chantier chantier) {
        this.chantier = chantier;
    }

    public int deplacer() {
        int q = 0;
        if (chantier.getNum() <= 0) {
            return q;
        }
        voyage++;
        synchronized (chantier) {
            q = chantier.get(20);
        }
        System.out.print(LocalTime.now());
        System.out.printf(": Brouette:%d/Voyage:%d/Reste:%d briques%n",
                this.id, this.voyage, chantier.getNum());
        return q;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVoyage() {
        return voyage;
    }

    public void setVoyage(int voyage) {
        this.voyage = voyage;
    }

    public Chantier getChantier() {
        return chantier;
    }

    public void setChantier(Chantier chantier) {
        this.chantier = chantier;
    }
}
