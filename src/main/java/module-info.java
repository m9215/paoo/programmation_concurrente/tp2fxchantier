module com.example.tp2fxchantier {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;


    opens paoo.pc.tp2.tp2fxchantier to javafx.fxml;
    exports paoo.pc.tp2.tp2fxchantier;
}